import React, { useState, useEffect } from 'react';
import FormInput from './formInput';
import RadioGroup from './formRadio';

const Form = () => {
  
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    age: '',
    employed: false,
    favoriteColor: '',
    sauces: [],
    bestStooge: 'Larry',
    note: '',
  });



  const [isResetDisabled, setIsResetDisabled] = useState(true);
  const [errors, setErrors] = useState({});
  const [inputClasses, setInputClasses] = useState({
    firstName: '',
    lastName: '',
    age: '',
    note: '',
  });

  function hasAllEmptyKeys(obj) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key) && obj[key]) {
        return false;
      }
    }
    return true;
  }


  
  const handleChange = (event) => {
    setIsResetDisabled(false);
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;


    //==============

    let isValid = true;
    let errorMessage = '';

    if (name === 'firstName' || name === 'lastName') {
      if (!/^[a-zA-Z\s]+$/.test(value)) {
        isValid = false;
        errorMessage = 'Only alphabets and spaces are allowed';
        console.log('Only alphabets and spaces are allowed')

      }
    } else if (name === 'age') {
      if (!/^\d+$/.test(value)) {
        isValid = false;
        errorMessage = 'Only numbers are allowed';
        console.log('Only numbers are allowed');

      }
    } else if (name === 'note') {
      if (value.length > 100) {
        isValid = false;
        errorMessage = 'Note should not exceed 100 characters';
        console.log('Note should not exceed 100 characters');

      }
    }
    setInputClasses({
      ...inputClasses,
      [name]: isValid ? '' : 'error',
    });

    setErrors({
      ...errors,
      [name]: errorMessage,
      
    });
    console.log(errors.note)

    //===============
  
    if (name === 'sauces') {
      const isChecked = target.checked;
      const selectedSauces = [...formData.sauces];
  
      if (isChecked) {
        selectedSauces.push(target.value);
         } else {
        const index = selectedSauces.indexOf(target.value);
        if (index > -1) {
          selectedSauces.splice(index, 1);
        }
      }
  
      setFormData({
        ...formData,
        [name]: selectedSauces,
      });
    } else {
      setFormData({
        ...formData,
        [name]: value,
      });
    }
  };
  
  
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(errors);
    if (hasAllEmptyKeys(errors)){
    const message = JSON.stringify(
      Object.keys(formData)
        .filter(key => {
          if (key === 'sauces' && Array.isArray(formData[key]) && formData[key].length === 0) {
            return false;
          }
          return formData[key] !== undefined && formData[key] !== '';
        })
        .reduce((obj, key) => {
          obj[key] = formData[key];
          return obj;
        }, {}),
      null,
      2
    )
    alert(message);
      }
   
  };

  const handleReset = () => {
    setIsResetDisabled(true);
    setInputClasses({
      firstName: '',
      lastName: '',
      age: '',
      note: '',
    });
    if (formData.employed) {
      document.getElementById('employed').checked = false;
    }
    setFormData({
      firstName: '',
      lastName: '',
      age: '',
      employed: false,
      favoriteColor: '',
      sauces: [],
      bestStooge: 'Larry',
      note: '',
    });
    

  };

  return (
    <div style={{ maxWidth: '600px', margin: '30px auto',  borderStyle: 'solid', borderWidth: '1px'}}>
      <form onSubmit={handleSubmit} onReset={handleReset}>
        <FormInput
        label="First Name:"
        id="firstName"
        name="firstName"
        type="text"
        value={formData.firstName}
        handleChange={handleChange}
        className={inputClasses.firstName === 'error' ? "error" : "inputName"}
        />
      {/* Render error message if there is one */}
      {inputClasses.firstName && <div className="error-message">{errors.firstName}</div>}
      {/* Render other FormInput components */}

        <FormInput
        label="Last Name:"
        id="lastName"
        name="lastName"
        type="text"
        value={formData.lastName}
        handleChange={handleChange}
        className={inputClasses.lastName === 'error' ? "error" : "inputName"}
        />
      {/* Render error message if there is one */}
      {inputClasses.lastName && <div className="error-message">{errors.lastName}</div>}
      {/* Render other FormInput components */}


        <FormInput
        label="Age:"
        id="age"
        name="age"
        type="text"
        value={formData.age}
        handleChange={handleChange}
        className={inputClasses.age === 'error' ? "error" : "inputName"}
        />
      {/* Render error message if there is one */}
      {inputClasses.age && <div className="error-message">{errors.age}</div>}
      {/* Render other FormInput components */}

        <FormInput
        label="Employed:"
        id="employed"
        name="employed"
        type="checkbox"
        checked={formData.employed}
        handleChange={handleChange}
        className={'inputName'}
        />

        <div>      
        <label  className='colorLabel' htmlFor="favoriteColor">Favorite Color:</label>
        <select
          id="favoriteColor"
          name="favoriteColor"
          value={formData.favoriteColor}
          onChange={handleChange}
          className='colorDrop'>
          <option value="">Please select</option>
          <option value="red">Red</option>
          <option value="blue">Blue</option>
          <option value="green">Green</option>
          <option value="yellow">Yellow</option>
        </select>
        </div>
        

        <br />

        <label htmlFor="sauces">Sauces:</label>
        <div>
          <input
            type="checkbox"
            id="ketchup"
            name="sauces"
            value="Ketchup"
            checked={formData.sauces.includes('Ketchup')}
            onChange={handleChange}
            className='sauceCheckBox'
          />
          <label htmlFor="ketchup">Ketchup</label>
        </div>
        <div>
          <input
            type="checkbox"
            id="mustard"
            name="sauces"
            value="Mustard"
            checked={formData.sauces.includes('Mustard')}
            onChange={handleChange}
            className='sauceCheckBox'
          />
          <label htmlFor="mustard">Mustard</label>
        </div>
        <div>
          <input
            type="checkbox"
            id="mayonnaise"
            name="sauces"
            value="Mayonnaise"
            className='sauceCheckBox'
            checked={formData.sauces.includes('Mayonnaise')}
            onChange={handleChange}
          />
          <label htmlFor="mayonnaise">Mayonnaise</label>
        </div>
        <div>
      <input
        type="checkbox"
        id="guacamole"
        name="sauces"
        value="Guacamole"
        className='sauceCheckBox'
        checked={formData.sauces.includes('Guacamole')}
        onChange={handleChange}
      />
      <label htmlFor="guacamole">Guacamole</label>
    </div>

    <br />
 
        <RadioGroup
        label="Best Stooge:"
        name="bestStooge"
        options={[
            { label: 'Larry', value: 'Larry' },
            { label: 'Moe', value: 'Moe' },
            { label: 'Curly', value: 'Curly' },
        ]}
        value={formData.bestStooge}
        onChange={handleChange}
        className='radioGroup'
        />

    <br />

    <label htmlFor="note">Note:</label>
    <textarea
      id="note"
      name="note"
      value={formData.note}
      onChange={handleChange}
      className={inputClasses.note}
    ></textarea>


         {/* Render error message if there is one */}
         {inputClasses.note && <div className="error-message">{errors.note}</div>}
      {/* Render other FormInput components */}
      
    <br />
    <div className='buttons'>
    <button type="submit">Submit</button>
    <button type="reset" onClick={handleReset} disabled={isResetDisabled}>Reset</button>
    </div>
  </form>

  <div style={{ backgroundColor: 'lightgray', padding: '10px', marginLeft: '50px', marginRight: '50px', marginTop: '20px', marginBottom: '40px' }}>
  <pre  style={{ whiteSpace: 'pre-wrap', wordWrap: 'break-word'}}>
    {JSON.stringify(
      Object.keys(formData)
        .filter(key => {
          if (key === 'sauces' && Array.isArray(formData[key]) && formData[key].length === 0) {
            return false;
          }
          return formData[key] !== undefined && formData[key] !== '';
        })
        .reduce((obj, key) => {
          obj[key] = formData[key];
          return obj;
        }, {}),
      null,
      2
    )}
  </pre>
</div>
</div>
);
};

export default Form;