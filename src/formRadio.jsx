const RadioGroup = ({ label, name, options, value, onChange }) => {
    return (
      <div>
        <label>{label}</label>
        {options.map((option) => (
          <div key={option.value}>
            <input
              type="radio"
              id={option.value}
              name={name}
              value={option.value}
              checked={value === option.value}
              onChange={onChange}
            />
            <label htmlFor={option.value}>{option.label}</label>
          </div>
        ))}
      </div>
    );
  };

  export default RadioGroup