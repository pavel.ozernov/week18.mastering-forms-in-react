import React from 'react';

const FormInput = ({ label, id, name, type, value, handleChange, className }) => {
  return (
    <div className="form-row">
      <label htmlFor={id}>{label}</label>
      <input
        type={type}
        id={id}
        name={name}
        value={value}
        onChange={handleChange}
        className={className}
      />
    </div>
  );
};

export default FormInput;